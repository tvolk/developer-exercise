class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    # used for making sure the special chars are not emiited
    special = "?,."
    regex = /[#{special.gsub(/./){|char| "\\#{char}"}}]/
    
    # split the string into an array
    words = str.scan(/\S+/)
    final = ""
    for i in words
        if i.length > 4
            if isUpper(i[0,1])
                final.concat("Marklar")
            else
                final.concat("marklar")
            end
            
            # makes sure to append any special characters onto
            # the end of marklar when 
            if i =~ regex
                value = i[-1,1]
                final.concat(value)
            end
        else
            final.concat(i) 
        end
        final.concat(" ")
    end

    # strips the trailing space off
    final = final.rstrip
    return final
  end
   
  # checks that a value is uppercase
  def self.isUpper(c)
      return c == c.upcase
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    # TODO: Implement this method
    evenArray = []
    fibArray = []

    n = 0
    while n < nth
        if n <= 1
            fibArray.push(n)
        else
            x = n - 1
            y = n - 2
            value = ( fibArray[x] + fibArray[y])
            fibArray.push(value)
            if (value % 2) == 0
                evenArray.push(value)
            end
        end
        n += 1
    end

    summedValue = evenArray.inject(:+)
    return summedValue
  end
end
